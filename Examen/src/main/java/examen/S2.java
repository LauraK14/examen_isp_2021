package examen;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class S2 extends JFrame implements ActionListener {

    private final JTextField textField1;
    private final JButton button;
    private final JTextField textField2;

    public S2() {
        setTitle("Subiectul 2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(580, 350);
        setLocationRelativeTo(null);
        setResizable(false);

        Container c = getContentPane();
        c.setLayout(null);

        textField1 = new JTextField();
        textField1.setFont(new Font("Arial", Font.PLAIN, 15));
        textField1.setSize(250, 20);
        textField1.setLocation(165, 90);
        textField1.setVisible(true);
        c.add(textField1);

        button = new JButton("Mutare text");
        button.setFont(new Font("Arial", Font.PLAIN, 15));
        button.setSize(275, 20);
        button.setLocation(150, 130);
        button.addActionListener(this);
        c.add(button);

        textField2 = new JTextField();
        textField2.setFont(new Font("Arial", Font.PLAIN, 15));
        textField2.setSize(250, 20);
        textField2.setLocation(165, 170);
        textField2.setEditable(false);
        textField2.setVisible(true);
        c.add(textField2);

        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == button) {
            textField2.setText(textField1.getText());
            textField1.setText("");
        }
    }

    public static void main(String[] args) {
        new S2();
    }
}