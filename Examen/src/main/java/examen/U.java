package examen;

class U {
    public void p() {  //am considerat pentru metoda p() tipul void

    }
}

class I extends U {
    private long t;
    K k;  //pentru asociere unidirectionala

    public void f() {  //am considerat pentru metoda f() tipul void

    }
}

class J {
    public void i(I I) {  // pentru relatia de dependenta

    }
}

class K {
    L l;
    M m;  // pentru agregare slaba

    K() {
        L l = new L();   // pentru agregare puternica
    }
}

class L {
    public void metA() { //am considerat pentru metoda metA() tipul void

    }
}

class M {
    public void metB() { //am considerat pentru metoda metB() tipul void

    }
}

class N {
    I i; //pentru asociere unidirectionala
}
